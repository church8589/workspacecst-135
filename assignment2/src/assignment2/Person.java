package assignment2;
/**
 * @author Andrew Wilkerson
 * GCU CST-135
 * This is my own original Work.
 *
 */

public class Person {
	
	private int age;
	private String firstName;
	private String lastName;
	private float weight;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person person = new Person(34, "Andrew", "Wilkerson", (float)205.3);
		System.out.printf("My name is %s %s %n", person.getFirstName(), person.getLastName());
		person.walk();
		person.run(10);
		

	}
	
	/**
	 * Creates a Person with a first, last name, age and weight.	
	 * @param age Person's age in years
	 * @param firstName Person's first name
	 * @param lastName Person's last name
	 * @param weight Person's weight in pounds
	 */
	
	public Person(int age, String firstName, String lastName, float weight) {
		this.setAge(age);
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setWeight(weight);
		
	}
	
	/**
	 * @param Prints out that the person is walking
	 */
	public void walk() {
		System.out.println("I am in walk()");
	}
	
	
	/**
	 * 
	 * @param distance Is how far the person is running
	 * @return Returns Zero right now as a place holder
	 */
	public float run(float distance) {
		System.out.printf("I am in run()%n");
		return 0;
	}

	/**
	 * 
	 * @return Person's Age in years
	 */
	public int getAge() {
		return age;
	}

	/**
	 * 
	 * @param age Set's Persons age in years
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * 
	 * @return Person's First name
	 */
	public String getFirstName() {
		return firstName;
	}
	

	/**
	 * 
	 * @param firstName Set's Person's first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * 
	 * @return Person's Last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * 
	 * @param lastName Sets Person's last name 
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * 
	 * @return Person's weight in pounds
	 */
	public float getWeight() {
		return weight;
	}

	/**
	 * 
	 * @param weight Sets Person's weight in pounds
	 */
	public void setWeight(float weight) {
		this.weight = weight;
	}
	
	

}
