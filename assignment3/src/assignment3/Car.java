package assignment3;
/**
 * 
 * @author Andrew Wilkerson
 *
 */
public class Car {
    private int speed = 0;
    private Tire[] tires;
    private Engine engine = new Engine();

    /**
     * Default constructor creating a car object with four tires at 0 psi
     * and engine turned off
     */
    public Car(){
        tires = new Tire[4];
        this.addTires();
        for(Tire tire: tires){
            tire.setPsi(0);
        }
        this.engine.setEngineOn(false);
    }

    /**
     * Creates Car with 4 tires at psi specified and engine turned off
     * @param psi Sets psi for each tire on car
     * 
     */
    public Car(int psi){
        tires = new Tire[4];
        this.addTires();
        for(Tire tire: tires){
            tire.setPsi(psi);
        }
        this.engine.setEngineOn(false);

    }

    /**
     * 
     * @return If tires are in correct state for driving
     */
    public boolean checkTires(){
        boolean tiresReady = true;
        for(Tire tire: tires){
            if(tire.getPsi() < 32){
                tiresReady = false;
                break;
            }
        }
        return tiresReady;
    }

    /**
     * 
     * @param speed Sets car to speed if all of the requirements are met
     * 
     */
    public void setSpeed(int speed){
        if(this.getEngineOn() && this.checkTires()){
            if(speed >= 0 && speed <= 60){
                this.speed = speed;
                System.out.println("The car is now travelling at " + this.speed + " mph.");
            }
            else{
                System.out.println("Please check speed entered (Must be >= 0 and >=60).");
            }
            
        }
        else{
            System.out.println("Please check the tires and engine before setting a speed.");
        }
    }

    /**
     * 
     * @return The current speed of Car
     */
    public int getSpeed(){
        return this.speed;
    }

    /**
     * 
     * @param psi Sets the psi of each tire on the Car
     */
    public void setTirePressure(int psi){
        for(Tire tire: tires){
            tire.setPsi(psi);
        }
    }
    
    /**
     * 
     * @return Prints out the tire pressure of each tire to the console
     */
    public String getTirePressure(){
        String tirePressure = "";
        int tireNumber = 0;
        for(Tire tire: tires){
            tirePressure += "Tire " + tireNumber + " is at " + Integer.toString(tire.getPsi()) + " psi.\n";
            tireNumber++;
        }

        return tirePressure;
    }

    /**
     * 
     * @return If the engine is on or not
     */
    public boolean getEngineOn(){
        return engine.getEngineOn();
    }

    /**
     * Turns the engine on if the tires are at the correct psi
     * otherwise prints out error message to console
     */
    public void turnEngineOn(){
        if(checkTires()){
            engine.setEngineOn(true);
            System.out.println("Engine is now turned on.");
        }
        else{
            System.out.println("Check tire pressure and try again.");
        }
    }

    /**
     * Checks to see if Car has come to a stop and turns engine off
     * if Car is still moving, prints error message to console
     */
    public void turnEngineOff(){
        if(this.speed == 0){
            engine.setEngineOn(false);
            System.out.println("Engine is now turned off.");
        }
        else{
            System.out.println("Cannont turn off engine while moving.");
        }
    }


    /**
     * used by the constructor to add four tires to Car
     */
    private void addTires(){
        for(int i = 0; i < tires.length; i++){
            Tire tire = new Tire();
            tires[i] = tire;
        }
    }

}
