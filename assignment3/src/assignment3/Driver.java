package assignment3;

public class Driver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car car = new Car(31);

        System.out.println(car.getTirePressure());
        car.setSpeed(35);
        car.setTirePressure(33);
        System.out.println(car.getTirePressure());
        car.turnEngineOn();
        car.setSpeed(60);
        car.turnEngineOff();
        car.setSpeed(0);
        car.turnEngineOff();

	}

}
