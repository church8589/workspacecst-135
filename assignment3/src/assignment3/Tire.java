package assignment3;
/**
 * 
 * @author Andrew Wilkerson
 *
 */
public class Tire {
	private int psi;

	/**
	 * Creates tire with default psi set to 0
	 */
    public Tire(){
        this.psi = 0;
    }

    /**
     * 
     * @param psi Sets psi of tire
     */
    public void setPsi(int psi){
        this.psi = psi;
    }

    /**
     * 
     * @return psi of tire
     */
    public int getPsi(){
        return psi;
    }
}
