package assignment3;
/**
 * 
 * @author Andrew Wilkerson
 *
 */
public class Engine {

	private boolean engineOn;

	/**
	 * Default constructor that sets engine to off
	 */
    public Engine(){
        this.engineOn = false;
    }

    /**
     * Turns engineOn to true or false
     * @param onOrOff True/false for either state
     */
    public void setEngineOn(boolean onOrOff){
        engineOn = onOrOff;
    }

    /**
     * 
     * @return Whether the engine is on or off
     */
    public boolean getEngineOn(){
        return engineOn;
    }
}
